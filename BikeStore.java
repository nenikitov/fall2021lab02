// Mykyta Onipchenko
// 2034746

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[] {
            new Bicycle("AMD", 64, 128.0),
            new Bicycle("Giant Bicycle", 2, 31.5),
            new Bicycle("Cannondale", 7, 17.3),
            new Bicycle("Schwinn", 1, 10.2),
        };

        for (int i = 0; i < bicycles.length; i++) {
            System.out.println(i + ":\n" + bicycles[i]);
        }
    }
}
